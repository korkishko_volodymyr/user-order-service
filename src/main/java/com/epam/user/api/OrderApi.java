package com.epam.user.api;

import com.epam.user.dto.OrderDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Order management REST API")
@ApiResponses({
        @ApiResponse(code = 404, message = "Not found"),
        @ApiResponse(code = 500, message = "Internal Server Error")
})
@RequestMapping("/api/v1/order")
public interface OrderApi {

    @ApiOperation("Get order API")
    @ApiResponse(code = 200, message = "OK", response = OrderDto.class)
    @GetMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    OrderDto getOrder(@PathVariable Long id);

    @ApiOperation("Create order API")
    @ApiResponse(code = 201, message = "Created", response = OrderDto.class)
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    OrderDto createOrder(@RequestBody OrderDto orderDto);

    @ApiOperation("Update order API")
    @ApiResponse(code = 200, message = "OK", response = OrderDto.class)
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    OrderDto updateOrder(@RequestBody OrderDto orderDto);

    @ApiOperation("Delete order API")
    @ApiResponse(code = 200, message = "OK")
    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    void deleteOrder(@PathVariable Long id);
}
