package com.epam.user.exception;

import com.epam.user.model.enums.ErrorCode;
import com.epam.user.model.enums.ErrorType;

public class UnableToGetUserException extends AbstractException {

  public UnableToGetUserException(String message, Throwable cause) {
    super(message, cause);
  }

  @Override
  public ErrorCode getErrorCode() {
    return ErrorCode.APPLICATION_ERROR_CODE;
  }

  @Override
  public ErrorType getErrorType() {
    return ErrorType.PROCESSING_ERROR_TYPE;
  }
}
