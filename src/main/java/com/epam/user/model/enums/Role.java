package com.epam.user.model.enums;

public enum Role {
  ROLE_USER, ROLE_ADMIN
}
