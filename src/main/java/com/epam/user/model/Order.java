package com.epam.user.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_table")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    //task finished
    private int price;
    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
}
