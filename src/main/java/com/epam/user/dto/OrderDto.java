package com.epam.user.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "Order data")
public class OrderDto {

    private Long id;
    private String code;
    private int price;
    private Long userId;
}
