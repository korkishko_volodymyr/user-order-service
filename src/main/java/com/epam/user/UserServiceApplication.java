package com.epam.user;

import com.epam.user.dto.UserDto;
import com.epam.user.model.enums.Role;
import com.epam.user.service.AuthService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class UserServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(UserServiceApplication.class, args);
  }

  @Bean
  public CommandLineRunner demoAdmin(AuthService authService,
      @Value("${app.auth.admin.password}") String password,
      @Value("${app.auth.admin.email}") String email) {
    return args -> {
      UserDto userDto = new UserDto();
      userDto.setName("Admin");
      userDto.setEmail(email);
      userDto.setPassword(password);
      authService.signUp(userDto, Role.ROLE_ADMIN);
    };
  }

}
