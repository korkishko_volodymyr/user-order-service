package com.epam.user.service;

import com.epam.user.dto.OrderDto;

public interface OrderService {

    OrderDto getOrder(Long orderId);

    OrderDto createOrder(OrderDto order);

    OrderDto updateOrder(OrderDto order);

    void deleteOrder(Long orderId);
}
