package com.epam.user.service.mapping;

import com.epam.user.dto.OrderDto;
import com.epam.user.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {

    OrderMapper ORDER_MAPPER = Mappers.getMapper(OrderMapper.class);

    @Mapping(source = "user.id", target = "userId")
    OrderDto orderToOrderDto(Order order);

    @Mapping(target = "user.id", source = "userId")
    Order orderDtoToOrder(OrderDto orderDto);
}
