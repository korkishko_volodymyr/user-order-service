package com.epam.user.service;

import com.epam.user.dto.UserDto;
import com.epam.user.model.enums.Role;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthService extends UserDetailsService {

  UserDto signIn(UserDto userDto);

  UserDto signUp(UserDto userDto, Role role);

  void signOut();

}
