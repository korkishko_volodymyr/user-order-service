package com.epam.user.service;

import com.epam.user.dto.UserDto;
import java.util.List;

public interface AdminService {

  List<UserDto> getAllUsers();

}
