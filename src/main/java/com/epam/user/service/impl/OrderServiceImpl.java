package com.epam.user.service.impl;

import com.epam.user.dto.OrderDto;
import com.epam.user.model.Order;
import com.epam.user.repository.OrderRepository;
import com.epam.user.service.OrderService;
import com.epam.user.service.mapping.OrderMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Override
    public OrderDto getOrder(Long orderId) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Order was not found"));
        log.info("getOrder: about to get with id {}", orderId);
        return OrderMapper.ORDER_MAPPER.orderToOrderDto(order);
    }

    @Override
    public OrderDto createOrder(OrderDto orderDto) {
        Order order = OrderMapper.ORDER_MAPPER.orderDtoToOrder(orderDto);
        order = orderRepository.save(order);
        log.info("createOrder: about to create order {}", orderDto);
        return OrderMapper.ORDER_MAPPER.orderToOrderDto(order);
    }

    @Override
    public OrderDto updateOrder(OrderDto orderDto) {
        log.info("updateOrder: about to update order {}", orderDto);
        return createOrder(orderDto);
    }

    @Override
    public void deleteOrder(Long orderId) {
        log.info("deleteOrder: about to delete order with id {}", orderId);
        orderRepository.deleteById(orderId);
    }
}
