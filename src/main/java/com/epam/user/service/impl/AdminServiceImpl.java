package com.epam.user.service.impl;

import com.epam.user.dto.UserDto;
import com.epam.user.repository.UserRepository;
import com.epam.user.service.AdminService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

  private final UserRepository userRepository;

  @Override
  public List<UserDto> getAllUsers() {
    log.info("getAllUsers: method is called");
    List<UserDto> allUsers = userRepository.findAll().stream()
        .map(UserServiceImpl::mapUserToUserDto)
        .collect(Collectors.toList());
    //CHANGE
    //asdqwwq
    //sadSADdwqe
    log.info("getAllUsers: {} users found", allUsers.size());
    return allUsers;
  }

}
