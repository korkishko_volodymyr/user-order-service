package com.epam.user.service.impl;

import static com.epam.user.service.impl.UserServiceImpl.mapUserDtoToUser;
import static com.epam.user.service.impl.UserServiceImpl.mapUserToUserDto;

import com.epam.user.dto.UserDto;
import com.epam.user.model.User;
import com.epam.user.model.enums.Role;
import com.epam.user.repository.UserRepository;
import com.epam.user.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final AuthenticationManager authenticationManager;

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    return userRepository.findByEmail(email)
        .orElseThrow(() -> new UsernameNotFoundException("Unable to find user email!"));
  }

  @Override
  public UserDto signIn(UserDto userDto) {
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            userDto.getEmail(),
            userDto.getPassword())
    );
    SecurityContextHolder.getContext().setAuthentication(authentication);

    User user = (User) authentication.getPrincipal();
    return mapUserToUserDto(user);
  }

  @Override
  public UserDto signUp(UserDto userDto, Role role) {
    User user = mapUserDtoToUser(userDto);
    log.info("createUser: about to register a new user with email {}", user.getEmail());

    user.setRole(role);
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    user = userRepository.save(user);
    log.info("Used with id {} successfully registered", user.getId());

    return signIn(userDto);
  }

  @Override
  public void signOut() {
    SecurityContextHolder.clearContext();
  }

}
