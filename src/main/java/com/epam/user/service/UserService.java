package com.epam.user.service;

import com.epam.user.dto.UserDto;
import com.epam.user.model.User;

public interface UserService {

  UserDto updateUser(UserDto userDto);

  void deleteUser(User user);

  boolean isUserExistsWithEmail(String email);

}
