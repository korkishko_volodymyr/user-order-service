package com.epam.user.controller;

import com.epam.user.api.AdminApi;
import com.epam.user.controller.assebler.UserModelAssembler;
import com.epam.user.controller.model.UserModel;
import com.epam.user.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AdminController implements AdminApi {

  private final AdminService adminService;
  private final UserModelAssembler modelAssembler;

  @Override
  public CollectionModel<UserModel> getAllUsers() {
    return modelAssembler.toCollectionModel(adminService.getAllUsers());
  }

}
