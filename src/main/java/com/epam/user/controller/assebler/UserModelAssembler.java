package com.epam.user.controller.assebler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.epam.user.controller.AdminController;
import com.epam.user.controller.UserController;
import com.epam.user.controller.model.UserModel;
import com.epam.user.dto.UserDto;
import com.epam.user.model.User;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class UserModelAssembler extends RepresentationModelAssemblerSupport<UserDto, UserModel> {

  public static final String GET_REL = "get";
  public static final String UPDATE_REL = "update";
  public static final String DELETE_REL = "delete";

  public UserModelAssembler() {
    super(UserController.class, UserModel.class);
  }

  @Override
  public UserModel toModel(UserDto entity) {
    UserModel userModel = new UserModel(entity);

    Link getLink = linkTo(methodOn(UserController.class).getUser(new User())).withRel(GET_REL);
    Link updateLink = linkTo(methodOn(UserController.class).updateUser(null)).withRel(UPDATE_REL);
    Link deleteLink = linkTo(methodOn(UserController.class).deleteUser(new User()))
        .withRel(DELETE_REL);

    return userModel.add(getLink, updateLink, deleteLink);
  }

  public CollectionModel<UserModel> toCollectionModel(List<UserDto> userDtos) {
    List<UserModel> userModels = userDtos.stream()
        .map(this::toModel)
        .collect(Collectors.toList());

    Link self = linkTo((methodOn(AdminController.class).getAllUsers())).withSelfRel();
    return CollectionModel.of(userModels).add(self);
  }

}
