package com.epam.user.controller;

import static com.epam.user.service.impl.UserServiceImpl.mapUserToUserDto;

import com.epam.user.api.UserApi;
import com.epam.user.controller.assebler.UserModelAssembler;
import com.epam.user.controller.model.UserModel;
import com.epam.user.dto.UserDto;
import com.epam.user.model.User;
import com.epam.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

  private final UserService userService;
  private final UserModelAssembler modelAssembler;

  @Override
  public UserModel getUser(User user) {
    log.info("getUser: with id {}", user.getId());
    return modelAssembler.toModel(mapUserToUserDto(user));
  }

  @Override
  public UserModel updateUser(UserDto userDto) {
    log.info("updateUser: input userDto {}", userDto);
    return modelAssembler.toModel(userService.updateUser(userDto));
  }

  @Override
  public ResponseEntity<Void> deleteUser(User user) {
    log.info("deleteUser: with id {}", user.getId());
    userService.deleteUser(user);
    return ResponseEntity.noContent().build();
  }

}
