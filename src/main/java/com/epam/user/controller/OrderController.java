package com.epam.user.controller;

import com.epam.user.api.OrderApi;
import com.epam.user.dto.OrderDto;
import com.epam.user.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OrderController implements OrderApi {

    private final OrderService orderService;

    @Override
    public OrderDto getOrder(Long id) {
        log.info("getOrder: with id {}", id);
        return orderService.getOrder(id);
    }

    @Override
    public OrderDto createOrder(OrderDto orderDto) {
        log.info("createOrder: input orderDto {}", orderDto);
        return orderService.createOrder(orderDto);
    }

    @Override
    public OrderDto updateOrder(OrderDto orderDto) {
        log.info("updateOrder: input userOrder {}", orderDto);
        return orderService.updateOrder(orderDto);
    }

    @Override
    public void deleteOrder(Long id) {
        log.info("deleteOrder: with id {}", id);
        orderService.deleteOrder(id);
    }
}
