package com.epam.user.controller;

import com.epam.user.api.AuthApi;
import com.epam.user.controller.assebler.UserModelAssembler;
import com.epam.user.controller.model.UserModel;
import com.epam.user.dto.UserDto;
import com.epam.user.model.enums.Role;
import com.epam.user.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController implements AuthApi {

  private final AuthService authService;
  private final UserModelAssembler modelAssembler;

  @Override
  public UserModel signIn(UserDto userDto) {
    return modelAssembler.toModel(authService.signIn(userDto));
  }

  @Override
  public UserModel signUp(UserDto userDto) {
    return modelAssembler.toModel(authService.signUp(userDto, Role.ROLE_USER));
  }

  @Override
  public ResponseEntity<Void> signOut() {
    authService.signOut();
    return ResponseEntity.noContent().build();
  }

}
