package com.epam.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.epam.user.controller.model.UserModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class UserServiceApplicationTests {

  @LocalServerPort
  private int port;

  @Value("${app.auth.admin.password}")
  private String password;
  @Value("${app.auth.admin.email}")
  private String email;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  void contextLoads() {
    UserModel forObject = restTemplate.withBasicAuth(email, password)
        .getForObject("http://localhost:" + port + "/api/v1/user", UserModel.class);

    assertEquals(forObject.getUserDto().getEmail(), email);
  }

}
