package com.epam.user.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.epam.user.dto.UserDto;
import com.epam.user.model.User;
import com.epam.user.repository.UserRepository;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

  public static final String TEST_EMAIL = "testemail@email.com";

  @InjectMocks
  private UserServiceImpl userService;

  @Mock
  private UserRepository userRepository;

  @Test
  void loadUserByUsernameTest() {
    when(userRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.of(new User()));
    boolean userExistsWithEmail = userService.isUserExistsWithEmail(TEST_EMAIL);
    assertTrue(userExistsWithEmail);
  }

  @Test
  void deleteUserTest() {
    User testUser = createTestUser();
    doNothing().when(userRepository).delete(testUser);
    userService.deleteUser(testUser);

    verify(userRepository, times(1)).delete(testUser);
  }

  @Test
  void deleteUserWithExceptionTest() {
    doThrow(RuntimeException.class).when(userRepository).delete(any());
    assertThrows(RuntimeException.class,
        () -> userService.deleteUser(new User()));
  }

  @Test
  public void updateUserTest() {
    UserDto testUserDto = createTestUserDto();
    User testUser = createTestUser();
    when(userRepository.save(any())).thenReturn(testUser);

    UserDto userDto = userService.updateUser(testUserDto);

    assertThat(userDto, allOf(
        hasProperty("name", equalTo(testUserDto.getName())),
        hasProperty("surname", equalTo(testUserDto.getSurname())),
        hasProperty("email", equalTo(testUserDto.getEmail()))
    ));
  }

  private User createTestUser() {
    User user = new User();
    user.setName("TestName");
    user.setSurname("TestSurname");
    user.setEmail(TEST_EMAIL);
    return user;
  }

  private UserDto createTestUserDto() {
    UserDto userDto = new UserDto();
    try {
      BeanUtils.copyProperties(userDto, createTestUser());
    } catch (IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
    return userDto;
  }

}